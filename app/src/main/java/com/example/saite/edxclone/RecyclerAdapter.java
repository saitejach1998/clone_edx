package com.example.saite.edxclone;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by saite on 6/7/2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.CourseViewHolder> {


private Context mCtx;
private ArrayList<CourseDeatails> dealList;

public RecyclerAdapter(Context mCtx, ArrayList<CourseDeatails> dealList) {
        this.mCtx = mCtx;
        this.dealList = dealList;
        }

@Override
public CourseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.course_card, null);
        return new CourseViewHolder(view);
        }




public void onBindViewHolder(CourseViewHolder holder, int position) {
        CourseDeatails course = dealList.get(position);
        Glide.with(mCtx).load(course.course_image_url).into(holder.course_image);
        holder.course_details.setText(course.course_title);
        holder.course_completion_details.setText(course.getCourse_end_details());
        }

@Override
public int getItemCount() {
        return dealList.size();
        }

class CourseViewHolder extends RecyclerView.ViewHolder {

    TextView course_details, course_completion_details;
    ImageView course_image;


    public CourseViewHolder(View itemView) {
        super(itemView);
        course_details = itemView.findViewById(R.id.course_details);
        course_completion_details = itemView.findViewById(R.id.course_completion_details);
       course_image =  itemView.findViewById(R.id.course_image);

    }
}
}
