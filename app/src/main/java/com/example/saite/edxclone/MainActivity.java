package com.example.saite.edxclone;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private int[] tabIcons = {
            R.drawable.tabs_find_icon,
            R.drawable.tabs_list,
            R.drawable.tab_find_select,
            R.drawable.tabs_lists_select
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
/*

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
*/

        tabLayout = (TabLayout) findViewById(R.id.main_page_menu);
        tabLayout.addTab(createTab("Courses", tabIcons[1]));
        tabLayout.addTab(createTab("Discovery", tabIcons[0]));
        /*
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        */
        //tabLayout.setTabTextColors(Color.parseColor("#0075b4"), Color.parseColor("#ffffff"));
        View view1 = tabLayout.getTabAt(0).getCustomView();

        final TextView titletext = (TextView) findViewById(R.id.textView_title);
        ImageView imageView = (ImageView) view1.findViewById(android.R.id.icon);
        TextView selectedText = (TextView) view1.findViewById(android.R.id.text1);
        imageView.setImageResource(tabIcons[3]);
        selectedText.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryDark));


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                View view = tab.getCustomView();
                View view1 = tabLayout.getTabAt((1 - tab.getPosition())).getCustomView();
                TextView selectedText = (TextView) view.findViewById(android.R.id.text1);
                ImageView imageView = (ImageView) view.findViewById(android.R.id.icon);

                if (tab.getPosition() == 0) {

                    titletext.setText("Courses");
                } else {
                    titletext.setText("Discovery");
                }

                ImageView imageView1 = (ImageView) view1.findViewById(android.R.id.icon);
                imageView1.setImageResource(tabIcons[(tab.getPosition())]);
                imageView.setImageResource(tabIcons[(1 - tab.getPosition()) + 2]);
                selectedText.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryDark));


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                View view1 = tabLayout.getTabAt((tab.getPosition())).getCustomView();
                TextView selectedText1 = (TextView) view1.findViewById(android.R.id.text1);


                selectedText1.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.grey));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
            }
        });


    }

    private TabLayout.Tab createTab(String text, int icon) {
        TabLayout.Tab tab = tabLayout.newTab().setText(text).setIcon(icon).setCustomView(R.layout.custom_tab_item);

        // remove imageView bottom margin
        if (tab.getCustomView() != null) {
            ImageView imageView = (ImageView) tab.getCustomView().findViewById(android.R.id.icon);
            ViewGroup.MarginLayoutParams lp = ((ViewGroup.MarginLayoutParams) imageView.getLayoutParams());
            lp.bottomMargin = 0;
            imageView.requestLayout();
        }

        return tab;
    }

    class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {

            switch (position) {
                case 0:
                    CoursesFragment f = new CoursesFragment();
                    return f;
                case 1:
                    discoveryFrag d = new discoveryFrag();
                    return d;

                default:
                    CoursesFragment g = new CoursesFragment();
                    return g;

            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
}
