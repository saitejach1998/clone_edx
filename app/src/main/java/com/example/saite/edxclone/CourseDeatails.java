package com.example.saite.edxclone;

/**
 * Created by saite on 6/7/2018.
 */

public class CourseDeatails {
    String course_title;
    String course_image_url;
    String course_end_details;

    public CourseDeatails(String course_title, String course_image_url, String course_end_details) {
        this.course_title = course_title;
        this.course_image_url = course_image_url;
        this.course_end_details = course_end_details;
    }

    public String getCourse_title() {
        return course_title;
    }

    public void setCourse_title(String course_title) {
        this.course_title = course_title;
    }

    public String getCourse_image_url() {
        return course_image_url;
    }

    public void setCourse_image_url(String course_image_url) {
        this.course_image_url = course_image_url;
    }

    public String getCourse_end_details() {
        return course_end_details;
    }

    public void setCourse_end_details(String course_end_details) {
        this.course_end_details = course_end_details;
    }
}
